/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author lucas
 */
public class Retangulo implements FiguraComLados{
    private final double ladoA;
    private final double ladoB;
    private final String nome = "Quadrado";
    
    public Retangulo (double ladoA, double ladoB){
        this.ladoA = ladoA;
        this.ladoB = ladoB;
    }
    
    @Override
    public double getLadoMaior(){
        if (ladoA > ladoB) {
            return ladoA;
        }
        else
            return ladoB;
    }
    @Override
    public double getLadoMenor(){
        if (ladoA > ladoB) {
            return ladoB;
        }
        else
            return ladoA;
    }
    
    @Override
    public double getArea(){
        return ladoA*ladoB;
    }
    
    @Override
    public double getPerimetro(){
        return ((ladoA*2)+(ladoB*2));
    }
    
    @Override
    public String getNome(){
        return nome;
    }
}
