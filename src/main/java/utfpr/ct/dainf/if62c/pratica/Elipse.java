/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author lucas
 */
public class Elipse implements FiguraComEixos {

    private double eixo1;
    private double eixo2;
    private final String nome = "Elipse";

    public Elipse() {
    }

    public Elipse(double eixo1, double eixo2) {
        this.eixo1 = eixo1;
        this.eixo2 = eixo2;
    }

    public double getArea() {
        return Math.PI * eixo1 * eixo2;
    }

    public double getPerimetro() {
        return Math.PI * (3 * (eixo1 + eixo2) - Math.sqrt((3 * eixo1 + eixo2) * (eixo1 + 3 * eixo2)));
    }
    
    @Override
    public double getEixoMenor(){
        if (eixo1 >= eixo2){
            return eixo2;
        }
        else
            return eixo1;
    }
    
    @Override
    public double getEixoMaior(){
        if (eixo1 >= eixo2){
            return eixo1;
        }
        else
            return eixo2;
    }
    
    @Override
    public String getNome(){
        return nome;
    }

}
